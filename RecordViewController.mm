//
//  RecordViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "RecordViewController.h"
#import "FrequencyViewController.h"
#import <Foundation/Foundation.h>
#import "MainNavigationViewController.h"
#import "DBManager.h"
#import "RhythmClass.h"
#import "AppDelegate.h"

float tempo = 94.0f;
int caraouselIndex = 0;

@interface RecordViewController (){

    NSMutableArray *dronePickerArray, *countArray;
    BOOL clapFlag1, clapFlag2, clapFlag3, clapFlag4;
    int playFlag, stopFlag;
    int micCounter, endresult;
    
    // Sajiv Elements
    
    float sampleRate;
    NSString *duration;
    int seconds, minutes;
    int mCurrentScore;
    NSString *currentRythmName;
    
    NSError *audioSessionError;
    double peakPowerForChannel;
    NSString *currentMusicFileName;
    NSString *documentDir;
    NSString *newPath;
    AVAudioPlayer *myplayer;
    
    // Database related Tags or Flags
    NSString *musicDuration, *mergeFilePath;
    int inst1,inst2,inst3,inst4,vol1,vol2,vol3,vol4,bpm,timeStamp,t1,t2,t3,t4,t1Vol,t2Vol,t3Vol,t4Vol;
    
    NSMutableArray *rhythmArray;
    NSString *beatOneMusicFile, *beatTwoMusicFile;
    NSTimer *beatTimer;
    
    int counter,grayCounter;
}

@end

@implementation RecordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    _genreIdSelected = 1;
    bpmValue = 0;
    isStopped = 0;
    
   // self.listView = [[JTListView alloc]initWithFrame:CGRectMake(10, 10, self.view.frame.size.width, 90)];
    [self resetFlags];
    
    mCurrentScore = 94;
    peakPowerForChannel = 0.0f;
    micCounter = endresult = 0;
    playFlag = stopFlag = seconds = minutes = 0;
    [_recordView setHidden:YES];
    
    [_playTimerBtn setHidden:YES];
    [_playStopBtn setHidden:YES];
    
    _myPort = nil;
    audioSessionError = nil;
    
    //countArray = [[NSArray alloc]initWithObjects:@"First",@"Second",@"Third",@"Fourth",@"Fifth",@"Six",@"Seven",@"Eight",@"Nine",@"Ten",@"Eleven",@"Twelve",@"Thirteen",@"Fourteen",@"Fifteen",@"Sixteen",@"Seventeen",@"Eighteen",@"Nineteen",@"Twenty", nil];
    
  //  countArray = [[NSArray alloc]initWithObjects:@"Ska",@"Calypso",@"Dancehall",@"Ragga",@"Reggaeton", nil];

    
   
//    
//    for (int i = 0; i < [countArray count]; i++) {
//        
//        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake((i * 320), 5, 320, 80)];
//        lbl.backgroundColor = [UIColor clearColor];
//        lbl.textColor = [UIColor colorWithWhite:1.0 alpha:1.0];
//        lbl.font = [UIFont fontWithName:@"HelveticaNeue" size:28.0];
//        lbl.textAlignment = NSTextAlignmentCenter;
//        lbl.text = [countArray objectAtIndex:i];
//        lbl.tag = i;
//        
//        [_topScrollView addSubview:lbl];
//        
//    }
    
    
    [_volumeSlider setMinimumTrackTintColor:[UIColor whiteColor]];
    [_volumeSlider setMaximumTrackTintColor:[UIColor blackColor]];
    [_volumeSlider setMaximumTrackImage:[UIImage imageNamed:@"slidertrackLatestBlack.png"] forState:UIControlStateNormal];
    [_volumeSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderprogressLatest.png"] forState:UIControlStateNormal];
    [_volumeSlider setThumbImage:[UIImage imageNamed:@"slider-thumb.png"] forState:UIControlStateNormal];
    
//    _bpmSlider.minimumValue = 60.0f/tempo;
//    _bpmSlider.maximumValue = 240.0f/tempo;
    
    [_bpmSlider setThumbImage:[UIImage imageNamed:@"slider-thumb.png"] forState:UIControlStateNormal];
    [_bpmSlider setMinimumTrackTintColor:[UIColor whiteColor]];
    [_bpmSlider setMaximumTrackTintColor:[UIColor grayColor]];
    [_bpmSlider setMaximumTrackImage:[UIImage imageNamed:@"slidertrackLatest.png"] forState:UIControlStateNormal];
    [_bpmSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderprogressLatest.png"] forState:UIControlStateNormal];
    
    /*
    UITapGestureRecognizer *volumeGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [_volumeSlider addGestureRecognizer:volumeGesture];
    
    UITapGestureRecognizer *bpmGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sliderTapped:)];
    [_bpmSlider addGestureRecognizer:bpmGesture];
     */
    
    [_dronePickerView setHidden:NO];
 //   [_dronePickerLayer setHidden:NO];
    _dronePickerView.layer.cornerRadius = 20.0;
    
//    UISwipeGestureRecognizer *recognizer;
//    recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleUpSwipe)];
//    [recognizer setDirection:(UISwipeGestureRecognizerDirectionUp)];
//    [self.dronView.superview addGestureRecognizer:recognizer];
//    
//    UISwipeGestureRecognizer *downSwipe;
//    downSwipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleDownSwipe)];
//    [downSwipe setDirection:(UISwipeGestureRecognizerDirectionDown)];
//    [self.dronView.superview addGestureRecognizer:downSwipe];
    
//    UITapGestureRecognizer *toggleDronBtnGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(toggleDronBtn)];
//    toggleDronBtnGesture.delegate = self;
//    [self.dronView addGestureRecognizer:toggleDronBtnGesture];
    UITapGestureRecognizer *dismissPicker = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismissPicker:)];
    dismissPicker.delegate = self;
    
    [self.dronePickerView addGestureRecognizer:dismissPicker];
    dronePickerArray = [[NSMutableArray alloc]init];
    
    for (int i = 0; i < 50; i++) {
        [dronePickerArray addObjectsFromArray:@[@"Eb",@"E",@"F",@"Gb",@"G",@"A",@"Bb",@"B",@"C",@"Db",@"D"]];
    }
    //NSLog(@"the array count: %lu, and aarray: %@",(unsigned long)dronePickerArray.count,dronePickerArray);
    [_dronePickerView selectRow:258 inComponent:0 animated:NO];
    
//    droneArrayCurrentValue = 5;
//    _droneTopLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue-1];
//    _droneLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue];
//    _droneBottomLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue+1];
//    if ([self.view.superclass isSubclassOfClass:[UIPageViewController class]]) {
//        NSLog(@"this is pageview controller in rec class");
//    }
    
    _carousel.type = iCarouselTypeCoverFlow2;
    
    // Scrollview hidden for timebeing
    [_topScrollView setHidden:YES];
    
    // Sajiv Elements
    _musicPlayer = [MPMusicPlayerController applicationMusicPlayer];
    
    NSNotificationCenter* notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter addObserver:self
                           selector:@selector(handleVolumeChanged:)
                               name:MPMusicPlayerControllerVolumeDidChangeNotification
                             object:_musicPlayer];
    [_musicPlayer beginGeneratingPlaybackNotifications];
    
//    _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
//    [_bpmTxt setText:_bpmString];
//    [_bpmSlider setValue:mCurrentScore];
    
    [_volumeSlider setValue:_musicPlayer.volume * 10];
    
    _volumeView = [[MPVolumeView alloc] initWithFrame: CGRectMake(-100,-100,16,16)];
    _volumeView.showsRouteButton = NO;
    _volumeView.userInteractionEnabled = NO;
    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] addSubview:_volumeView];
    
    
    
    NSArray *pathComponents = [NSArray arrayWithObjects:
                               [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject],
                               @"MyAudioMemo.m4a",
                               nil];
    NSURL *outputFileURL = [NSURL fileURLWithPathComponents:pathComponents];
    
    NSMutableDictionary *recordSetting = [[NSMutableDictionary alloc] init];
    
    [recordSetting setValue:[NSNumber numberWithInt:kAudioFormatMPEG4AAC] forKey:AVFormatIDKey];
    [recordSetting setValue:[NSNumber numberWithFloat:44100.0] forKey:AVSampleRateKey];
    [recordSetting setValue:[NSNumber numberWithInt: 2] forKey:AVNumberOfChannelsKey];
    
    // Initiate and prepare the recorder
    _recorder = [[AVAudioRecorder alloc] initWithURL:outputFileURL settings:recordSetting error:nil];
    _recorder.meteringEnabled = YES;
    [_recorder prepareToRecord];
    
    
    
    _session = [AVAudioSession sharedInstance];
    [_session setCategory:AVAudioSessionCategoryPlayAndRecord error:nil];
    
    for (_input in [_session availableInputs]) {
        // set as an input the build-in microphone
        
        if ([_input.portType isEqualToString:AVAudioSessionPortBuiltInMic]) {
            _myPort = _input;
            break;
        }
    }
    
    [_session setPreferredInput:_myPort error:nil];
   // [_session setPreferredInput:_myPort error:&audioSessionError];
  //  [self setupApplicationAudio];
    [self registerForMediaPlayerNotifications];
    
    micArray = [[NSArray alloc]initWithObjects:mic1,mic2,mic3,mic4,mic5,mic6,mic7,mic8,mic9,mic10, nil];

    // Default values to be saved after app launches
    [self setDefaultValuesToMusicFiles];
    
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self fetchDBData];
    
    [_micView setHidden:YES];
    //  NSLog(@"RhythmArray = %@",[rhythmArray objectAtIndex:0]);
    [_carousel reloadData];
    
    if (_bpmDefaultFlag == 1) {
        NSLog(@"Index will appear ------ %d",(int)[_carousel currentItemIndex]);
        [self setDataToUIElements:(int)[_carousel currentItemIndex]];
    }
    
  //  [self.beatsView setHidden:NO];
    
}

// Fetching data from Database for the First time
-(void)fetchDBData{
    
    [countArray removeAllObjects];
    _genreIdSelected = [[_genreIdDict valueForKey:@"genreId"] intValue];
    if (_genreIdSelected == 0) {
        _genreIdSelected = 1;
    }
    
   // _bpmDefaultFlag = [[_genreIdDict valueForKey:@"bpmDefault"] intValue];
    NSLog(@"bpm = %d",_bpmDefaultFlag);
    
    DBManager *rhythmQuery = [[DBManager alloc]init];
    rhythmArray = [rhythmQuery getRhythmRecords:[NSNumber numberWithInt:_genreIdSelected]];
    
    for (RhythmClass *cls in rhythmArray) {
        NSString *str = [cls valueForKey:@"rhythmName"];
        [countArray addObject:str];
    }
}

-(void)setDefaultValuesToMusicFiles{
    
    inst1 = inst2 = 1;
    inst3 = inst4 = 0;
    vol1 = vol2 = vol3 = vol4 = 6;
    t1 = t2 = t3 = t4 = -1;
    t1Vol = t2Vol = t3Vol = t4Vol = -1;
    
    bpm = 60;
    timeStamp = 007;
    
    musicDuration = @"-1";
    mergeFilePath = @"-1";
}

- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    //self.items = [NSMutableArray array];
//    _items = [[NSMutableArray alloc]initWithObjects:@"One",@"Two",@"Three",@"Four",@"Five",@"Six",@"Seven",@"Eight",@"Nine",@"Ten",@"Eleven", nil];
  //  _dronePickerView.transform = CGAffineTransformMakeScale(.5, 0.5);
    NSLog(@"Awake from nib");
    countArray = [[NSMutableArray alloc]init];
    
    
    
}

-(void)restoreValuesAfterScreenSlide{
    
}

/*
- (void)sliderTapped:(UIGestureRecognizer *)g {
    UISlider* s = (UISlider*)g.view;
    if (s.highlighted)
        return; // tap on thumb, let slider deal with it
    CGPoint pt = [g locationInView: s];
    CGFloat percentage = pt.x / s.bounds.size.width;
    CGFloat delta = percentage * (s.maximumValue - s.minimumValue);
    CGFloat value = s.minimumValue + delta;
    [s setValue:value animated:YES];
    [s sendActionsForControlEvents:UIControlEventValueChanged];
}
 */

- (float) bpmForSelectedRythm:(NSString*)_rythm{
    
    for (RhythmClass *cls in rhythmArray) {
        NSString *str = [cls valueForKey:@"rhythmName"];
        if ([str isEqualToString:_rythm]) {
            return [[cls valueForKey:@"rhythmBPM"] floatValue];
        }
        
    }
    /*
    if ([_rythm isEqualToString:@"Ska"]) {
        tempo = 124;
        
    }else if ([_rythm isEqualToString:@"Calypso"]) {
        tempo = 120;
        
    }else if ([_rythm isEqualToString:@"Dancehall"]) {
        tempo = 98;
        
    }else if ([_rythm isEqualToString:@"Ragga"]) {
        tempo = 94;
        
    }else if ([_rythm isEqualToString:@"Reggaeton"]) {
        tempo = 95;
        
    }
    return tempo;
     */
    return 0;
}

- (void)handleVolumeChanged:(id)notification {
    [_volumeSlider setValue:_musicPlayer.volume * 10];
}

-(void)onPlayTimer:(NSTimer *)timer {
    if(seconds == 60)
    {
        seconds = 0;
        minutes++;
    }
    
    duration = [NSString stringWithFormat:@"%.2d:%.2d", minutes, seconds];
    // play timer change
    [_playTimerBtn setText:duration];
    
    // record timer value change
    [_recordTimerText setText:duration];
    seconds++;
    
    [self setVolumeInputOutput];
}

- (void) updateMicGain:(NSTimer *)timer {
    [_recorder updateMeters];
    
    peakPowerForChannel = pow(10, (0.05 * ([_recorder peakPowerForChannel:0] + [_recorder averagePowerForChannel:0])/2));
    
    endresult = int(peakPowerForChannel*100.0f);
    
    if(endresult > 0 && endresult <= 9)
    {
        [self calculateMicGain:0];
    }
    else if(endresult > 9 && endresult <= 10)
    {
        [self calculateMicGain:1];
    }
    else if(endresult > 10 && endresult <= 100)
    {
        [self calculateMicGain:endresult/10 + 1];
    }
    
    
}

-(void)setVolumeInputOutput{
    currentOutputs = _session.currentRoute.outputs;
    for( _output in currentOutputs )
    {
        if([_output.portType isEqualToString:AVAudioSessionPortHeadphones])
        {
            //cout << "                      AVAudioSessionPortHeadphones                          ";
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideNone error:nil];
            [_session setPreferredInput:_myPort error:nil];
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInSpeaker])
        {
            //cout << "                      AVAudioSessionPortBuiltInSpeaker                          ";
            break;
        }
        else if([_output.portType isEqualToString:AVAudioSessionPortBuiltInReceiver])
        {
            //cout << "                      AVAudioSessionPortBuiltInReceiver                          ";
            [_session overrideOutputAudioPort:AVAudioSessionPortOverrideSpeaker error:nil];
            break;
        }
    }
}

- (void) calculateMicGain:(int) gain
{
    
    if(gain == 0)
    {
        for(micCounter = 0;micCounter < 10;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:micCounter];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter]->setColor(Color3B(51, 51, 51));
        }
    }
    else if(gain > 0)
    {
        for(micCounter = 1;micCounter <= gain;micCounter++)
        {
            if(micCounter <= 7)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"green-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::GREEN);
            }
            else if(micCounter == 8 || micCounter == 9)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"orange-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::ORANGE);
            }
            else if(micCounter == 10)
            {
                UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
                [img setImage:[UIImage imageNamed:@"red-strip.png"]];
                //micarray[micCounter - 1]->setColor(Color3B::RED);
            }
        }
        for(micCounter = gain+1;micCounter < 11;micCounter++)
        {
            UIImageView *img = (UIImageView*)[micArray objectAtIndex:(micCounter - 1)];
            [img setImage:[UIImage imageNamed:@"grey_strips.png"]];
            //micarray[micCounter-1]->setColor(Color3B(51, 51, 51));
        }
    }
}



-(void)resetFlags{
    clapFlag3 = clapFlag4 = 0;
    clapFlag1 = clapFlag2 = 1;
}

-(void)viewDidLayoutSubviews{
    //[_topScrollView setContentSize:CGSizeMake(([countArray count] * 320), 80)];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
}

-(void)viewDidDisappear:(BOOL)animated{
    //[super viewDidAppear:animated];
    
    [self resetFlags];
    
    if(_audioPlayer.isPlaying) {
        playFlag = 0;
        _audioPlayer.currentTime = 0;
        [_audioPlayer stop];
        [_playTimerBtn setHidden:YES];
        [_playStopBtn setHidden:YES];
        [_stopBtn setEnabled:YES];
        [_playBtn setBackgroundImage:[UIImage imageNamed:@"play-button.png"] forState:UIControlStateNormal];
    }
    
    if(_audioPlayerClap1.isPlaying) {
        _audioPlayerClap1.currentTime = 0;
        [_audioPlayerClap1 stop];
    }
    
    if(_audioPlayerClap2.isPlaying) {
        _audioPlayerClap2.currentTime = 0;
        [_audioPlayerClap2 stop];
    }
    
    if(_audioPlayerClap3.isPlaying) {
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
    }
    
    if(_audioPlayerClap4.isPlaying) {
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
    }
    playFlag = 1;
    UIButton *btn;
    [self onTapPlayBtn:btn];
    // first Stop timer
    [beatTimer invalidate];
    beatTimer = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)toggleDronBtn {
//    [_dronePickerLayer setHidden:YES];
//    [_dronePickerView setHidden:YES];
//    NSLog(@"handle the single tap");
    UIButton *btn = (UIButton*)[self.view viewWithTag:44];
    [self onTapClap4Btn:btn];
    
}

//- (void)droneScroll {
//    UIScrollView *scroll = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, 60, 60)];
//    scroll.pagingEnabled = YES;
//    for (int i = 0; i < dronePickerArray.count; i++) {
//        NSString *str = dronePickerArray[i];
//        UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(0, 60*i, 60, 60)];
//        [scroll addSubview:lbl];
//        lbl.backgroundColor = [UIColor redColor];
//        lbl.textAlignment = NSTextAlignmentCenter;
//        lbl.text = str;
//    }
//    scroll.contentSize = CGSizeMake(60, 60*dronePickerArray.count);
//    [scroll setNeedsLayout];
//    [self.lblView addSubview:scroll];
//}

- (void)dismissPicker:(UITapGestureRecognizer*)gestureRecognizer {
    //    [_dronePickerLayer setHidden:YES];
    //    [_dronePickerView setHidden:YES];
    UIButton *btn = (UIButton*)[self.view viewWithTag:44];
    [self onTapClap4Btn:btn];
    NSLog(@"handle the single tap");
}
//- (void)handleUpSwipe {
//    NSLog(@"this is swipe action");
////    [_dronePickerLayer setHidden:NO];
////    [_dronePickerView setHidden:NO];
//    if (droneArrayCurrentValue == 10) {
//        droneArrayCurrentValue = -1;
////        return;
//    }
//    //    _droneTopLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue-1];
//    //    _droneLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue];
//    _droneBottomLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue+1];
//    
//    droneArrayCurrentValue++;
//    
//    NSLog(@"this is swipe action");
//    //    [_dronePickerLayer setHidden:NO];
//    //    [_dronePickerView setHidden:NO];
//    [UIView animateWithDuration:0.5 animations:^{
//        CGRect rect = _droneLabel.frame;
//        //        _droneTopLabel.frame = rect;
//        rect.size.height -= 60;
//        _droneTopLabel.frame = CGRectMake(0,-120,60, 60);
//        _droneLabel.frame = CGRectMake(0,-60,60, 60);
//        _droneBottomLabel.frame = CGRectMake(0,0,60, 60);
//    } completion:^(BOOL finished) {
//        _droneLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue];
//        //        _droneLabel.text = _droneBottomLabel.text;
//        [self resetDroneLbl];
//    }];
//
//}
//- (void)handleDownSwipe {
//    NSLog(@"this is down action");
//    if (droneArrayCurrentValue == 0) {
//        droneArrayCurrentValue = 11;
////        return;
//    }
//    _droneTopLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue-1];
//    //    _droneLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue];
//    //    _droneBottomLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue+1];
//    
//    droneArrayCurrentValue--;
//    
//    _droneTopLabel.hidden = NO;
//    [UIView animateWithDuration:0.5 animations:^{
//        CGRect rect = _droneLabel.frame;
//        //        _droneTopLabel.frame = rect;
//        rect.size.height -= 60;
//        _droneTopLabel.frame = CGRectMake(0,0,60, 60);
//        _droneLabel.frame = CGRectMake(0,60,60, 60);
//        _droneBottomLabel.frame = CGRectMake(0,120,60, 60);
//    } completion:^(BOOL finished) {
//        _droneLabel.text = [dronePickerArray objectAtIndex:droneArrayCurrentValue];
//        //        _droneLabel.text = _droneTopLabel.text;
//        [self resetDroneLbl];
//    }];
//}
//
//- (void)resetDroneLbl {
//    _droneTopLabel.frame = CGRectMake(0,-60,60, 60);
//    _droneLabel.frame = CGRectMake(0,0,60, 60);
//    _droneBottomLabel.frame = CGRectMake(0,60,60, 60);
//}

- (IBAction)onTapClap1Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag1 == 0) {
        [btn setSelected:YES];
        clapFlag1 = 1;
        if(playFlag == 1)
        {
            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
        _audioPlayerClap1.currentTime = 0;
        [_audioPlayerClap1 stop];
        clapFlag1 = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Clap_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap2Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag2 == 0) {
        [btn setSelected:YES];
        clapFlag2 = 1;
        if(playFlag == 1)
        {
            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
        _audioPlayerClap2.currentTime = 0;
        [_audioPlayerClap2 stop];
        clapFlag2 = 0;
        
    }
    // [btn setImage:[UIImage imageNamed:@"Claps2_Selected.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap3Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag3 == 0) {
        [btn setSelected:YES];
        clapFlag3 = 1;
        if(playFlag == 1)
        {
            [self playSelectedRhythms:currentRythmName];
        }
    } else {
        [btn setSelected:NO];
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
        clapFlag3 = 0;
    }
    //  [btn setImage:[UIImage imageNamed:@"Claps3_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapClap4Btn:(id)sender {
    UIButton *btn = (UIButton*)sender;
    
    if (clapFlag4 == 0) {
        [btn setSelected:YES];
        if(playFlag == 1)
        {
            [self playMemos:_droneType];
            _audioPlayerClap4.currentTime = 0;
            [_audioPlayerClap4 play];
        }
        
        clapFlag4 = 1;
    } else {
        [btn setSelected:NO];
        
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
        clapFlag4 = 0;
    }
    // [btn setImage:[UIImage imageNamed:@"Claps4_Blue.png"] forState:UIControlStateSelected];
}

- (IBAction)onTapPlayBtn:(id)sender {
    // Show 2 Labels
    
    if (playFlag == 0) {
        [_stopBtn setEnabled:NO];
        
        [_playBtn setBackgroundImage:[UIImage imageNamed:@"BlockNew.png"] forState:UIControlStateNormal];
        [_playTimerBtn setHidden:NO];
        [_playStopBtn setHidden:NO];
        
        [self playSelectedRhythms:currentRythmName];
        [_playTimerBtn setText:duration];
        
        _playTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                      target: self
                                                    selector:@selector(onPlayTimer:)
                                                    userInfo: nil repeats:YES];
        [_playTimer fire];
        
        playFlag = 1;
        
        // Call BeatMeter Image change method
        counter = 0;
        grayCounter = beatCount-1;
        for (int i = 0; i < 12; i++) {
            UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
            [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
        }
        [self changeBeatMeterImages];
        
    }else if(playFlag == 1){
        [_stopBtn setEnabled:YES];
        [_playBtn setBackgroundImage:[UIImage imageNamed:@"play-button.png"] forState:UIControlStateNormal];
        
        [_playTimerBtn setHidden:YES];
        [_playStopBtn setHidden:YES];
        
        [self stopSelectedRhythms];
        
        [_playTimer invalidate];
        _playTimer = nil;
        
        [_recordTimer invalidate];
        _recordTimer = nil;
        
        [beatTimer invalidate];
        beatTimer = nil;
        for (int i = 0; i < 12; i++) {
            UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
            [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
        }
        
        seconds = 0;
        minutes = 0;
        
        playFlag = 0;
    }
    
    if (stopFlag == 1) {
        [_playBtn setSelected:NO];
        [_stopBtn setSelected:NO];
        stopFlag = 0;
        playFlag = 0;
        
        [_recordView setHidden:YES];
        [_bpmView setHidden:NO];
        [_micView setHidden:YES];
    }
}

- (IBAction)onTapStopBtn:(id)sender {
    // change 2 buttons image
    if (stopFlag == 0) {
        [_playBtn setSelected:YES];
        [_stopBtn setSelected:YES];
        
        stopFlag = 1;
        playFlag = 1;
        
        [_recordView setHidden:NO];
        [_bpmView setHidden:YES];
        [_micView setHidden:NO];
        
        [self playSelectedRhythms:currentRythmName];
        
        [_recorder record];
        
        _playTimer = [NSTimer scheduledTimerWithTimeInterval: 0.01f
                                                      target: self
                                                    selector:@selector(updateMicGain:)
                                                    userInfo: nil repeats:YES];
        [_playTimer fire];
        
        _recordTimer = [NSTimer scheduledTimerWithTimeInterval: 1.0f
                                                      target: self
                                                    selector:@selector(onPlayTimer:)
                                                    userInfo: nil repeats:YES];
        [_recordTimer fire];

        
        
    }else if (stopFlag == 1) {
        [_playBtn setSelected:NO];
        [_stopBtn setSelected:NO];
        

        [_recorder stop];
        
        [_playTimer invalidate];
        _playTimer = nil;
        
        // to reset record timer text
        [_recordTimer invalidate];
        _recordTimer = nil;
        
        seconds = 0;
        minutes = 0;
        
        [self stopSelectedRhythms];
        
        // Show alert for save recording
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Save Recording" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Delete",@"Save", nil];
        alert.alertViewStyle = UIAlertViewStylePlainTextInput;
        [alert show];
        stopFlag = 0;
        [_recordView setHidden:YES];
        [_bpmView setHidden:NO];
        [_micView setHidden:YES];
    }
}

- (IBAction)onTapPlusBtn:(id)sender {
    if (mCurrentScore < 240) {
        
        mCurrentScore++;
        sampleRate = mCurrentScore;
        
        if(_audioPlayer.isPlaying) {
            _audioPlayer.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap1.isPlaying) {
            _audioPlayerClap1.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap2.isPlaying) {
            _audioPlayerClap2.rate = sampleRate/tempo;
        }
        if(_audioPlayerClap3.isPlaying) {
            _audioPlayerClap3.rate = sampleRate/60.0;
        }
        
        if(_audioPlayerClap4.isPlaying) {
            _audioPlayerClap4.rate = sampleRate;
        }
        
        _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
        [_bpmTxt setText:_bpmString];
        [_bpmSlider setValue:sampleRate];
        bpmValue = mCurrentScore;
        
        // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
        
        if (playFlag == 1) {
            [self changeBeatMeterImages];
        }
    }
}

- (IBAction)onTapMinusBtn:(id)sender {
    if (mCurrentScore > 60) {
        mCurrentScore--;
        
        sampleRate = mCurrentScore;
        
        if(_audioPlayer.isPlaying) {
            _audioPlayer.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap1.isPlaying) {
            _audioPlayerClap1.rate = sampleRate/tempo;
        }
        
        if(_audioPlayerClap2.isPlaying) {
            _audioPlayerClap2.rate = sampleRate/tempo;
        }
        if(_audioPlayerClap3.isPlaying) {
            _audioPlayerClap3.rate = sampleRate/60.0;
        }
        
        if(_audioPlayerClap4.isPlaying) {
            _audioPlayerClap4.rate = sampleRate/tempo;
        }
        
        _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
        [_bpmTxt setText:_bpmString];
        [_bpmSlider setValue:sampleRate];
        bpmValue = mCurrentScore;
        
        // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
        
        if (playFlag == 1) {
            [self changeBeatMeterImages];
        }
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"button Index =  %ld",(long)buttonIndex);
    if (buttonIndex == 1) {
        // Get the input text
        currentMusicFileName = [alertView textFieldAtIndex:0].text;
        //tempoCounter = NSString(audioPlayer.rate;
        time_t     now = time(0);
        struct tm  tstruct;
        char       date[80];
        char       time[80];
        tstruct = *localtime(&now);
        strftime(date, sizeof(date), "%d/%m/%Y", &tstruct);
        strftime(time, sizeof(time), "%X", &tstruct);
        
        currentMusicFileName=[currentMusicFileName stringByAppendingString:@".m4a"];
        [self renameFileName:@"MyAudioMemo.m4a" withNewName:currentMusicFileName];
        
//        NSString *tempoString = [NSString stringWithFormat:@"%d", (int)tempo];
//        
//        NSString *rateString = [NSString stringWithFormat:@"%f", _audioPlayer.rate];
        
        //NSString *dateString = [NSString stringWithCString:date
        //encoding:[NSString defaultCStringEncoding]];
        
        //NSString *timeString = [NSString stringWithCString:time
        //encoding:[NSString defaultCStringEncoding]];
        
        
        
        //someText=[someText stringByAppendingString:@".m4a"];
        
        
        //        currentMusicFileName=[currentMusicFileName stringByAppendingString:[NSString stringWithFormat:@"-%@", [_audioPlayer.url lastPathComponent]]];
        //        currentMusicFileName=[currentMusicFileName stringByAppendingString:[NSString stringWithFormat:@"-%@", rateString]];
        //        currentMusicFileName=[currentMusicFileName stringByAppendingString:[NSString stringWithFormat:@"-%@", tempoString]];
        //someText=[someText stringByAppendingString:[NSString stringWithFormat:@"-%@", dateString]];
        //someText=[someText stringByAppendingString:[NSString stringWithFormat:@"-%@", timeString]];
        
        AVAudioPlayer * sound = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:newPath] error:nil];
        NSLog(@"Duration = %f",sound.duration);
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat: @"yyyy-MM-dd"];
        
        NSString *stringFromDate = [dateFormatter stringFromDate:[NSDate date]];
        
        NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
        [timeFormatter setDateFormat: @"HH:mm:ss"];
        
        NSString *stringFromTime = [timeFormatter stringFromDate:[NSDate date]];


        // Have to change BPM value instaed of _bpmTxt.text in next line
        
        NSDictionary *musicDict = [[NSDictionary alloc]initWithObjectsAndKeys:currentMusicFileName,@"name",[NSString stringWithFormat:@"%d",(clapFlag1)],@"inst1",[NSString stringWithFormat:@"%d",(clapFlag2)],@"inst2",[NSString stringWithFormat:@"%d",(clapFlag3)],@"inst3",[NSString stringWithFormat:@"%d",(clapFlag4)],@"inst4",[NSString stringWithFormat:@"%d",vol1],@"vol1",[NSString stringWithFormat:@"%d",vol2],@"vol2",[NSString stringWithFormat:@"%d",vol3],@"vol3",[NSString stringWithFormat:@"%d",vol4],@"vol4",currentRythmName,@"rhythm",_bpmTxt.text,@"bpm",stringFromDate,@"date",stringFromTime,@"time",[NSString stringWithFormat:@"%f",sound.duration],@"duration",newPath,@"t1",@"-1",@"t2",@"-1",@"t3",@"-1",@"t4",@"6",@"t1vol",@"-1",@"t2vol",@"-1",@"t3vol",@"-1",@"t4vol",@"-1",@"mergefile",@"0",@"isDeleted", nil];
        
        NSLog(@"dict = %@",musicDict);
        
        DBManager *saveRhythm = [[DBManager alloc]init];
        [saveRhythm insertDataToRecordingInDictionary:musicDict];
        
        
        //  SavedListViewController *listView = [self.storyboard instantiateViewControllerWithIdentifier:@"thirdVCold"];
        // SavedListViewController *listView = [[SavedListViewController alloc]init];
        // listView.musicFileDict = musicDict;
        [_myNavigationController viewToPresent:2 withDictionary:musicDict];
        
        
        //   removeAllRecordListeners();
        //   addAllListeners();
        
    }
}

-(BOOL)renameFileName:(NSString*)oldname withNewName:(NSString*)newname{
    documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,
                                                       NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *oldPath = [documentDir stringByAppendingPathComponent:oldname];
    newPath = [documentDir stringByAppendingPathComponent:newname];
    
    
    
    
    
    NSFileManager *fileMan = [NSFileManager defaultManager];
    NSError *error = nil;
    
    if (![fileMan moveItemAtPath:oldPath toPath:newPath error:&error])
    {
        NSLog(@"Failed to move '%@' to '%@': %@", oldPath, newPath, [error localizedDescription]);
        return false;
    }
    
    return true;
}

- (IBAction)OnChangeVolumeSlider:(id)sender {
    _musicPlayer.volume = [_volumeSlider value]/10.0f;
}

- (IBAction)onChangeBPM:(id)sender {
    
    mCurrentScore = [_bpmSlider value];
    NSLog(@"mCurrentScore = %d",mCurrentScore);
    
    if(_audioPlayer.isPlaying) {
        _audioPlayer.rate = mCurrentScore/tempo;
    }
    
    if(_audioPlayerClap1.isPlaying) {
        _audioPlayerClap1.rate = mCurrentScore/tempo;
    }
    
    if(_audioPlayerClap2.isPlaying) {
        _audioPlayerClap2.rate = mCurrentScore/tempo;
    }
    if(_audioPlayerClap3.isPlaying) {
        _audioPlayerClap3.rate = mCurrentScore/60.0;
    }
    mCurrentScore = [_bpmSlider value];
    _bpmString = [NSString stringWithFormat:@"%d bpm", mCurrentScore];
    [_bpmTxt setText:_bpmString];
    bpmValue = mCurrentScore;
    
    // Call BeatMeter Image change method
        // first Stop timer
        [beatTimer invalidate];
        beatTimer = nil;
    if (playFlag == 1) {
        [self changeBeatMeterImages];
    }
}

-(void)changeBeatMeterImages{
    
    // Take Beat meter count and current BPM
    float beatFrequency = 60.0 / mCurrentScore;
    NSLog(@"beatFreq = %f",beatFrequency);
    
    if ([beatTimer isValid]) {
        [beatTimer invalidate];
       // beatTimer = nil;
    }
   
    beatTimer = [NSTimer scheduledTimerWithTimeInterval:beatFrequency target:self selector:@selector(changeBeatMeterImage) userInfo:nil repeats:YES];
    
//    while (isStopped == 0) {
//      //  for (int i = 0; i < beatCount; i++) {
//            UIImageView *beatImg = (UIImageView*)[self.beatsView viewWithTag:i];
//            [beatImg setImage:[UIImage imageNamed:@"beat_ball_green.png"]];
//            //[beatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
//      //  }
//        
//    }
    
}

-(void)changeBeatMeterImage{
    
    if(beatCount == counter)
    {
        counter = 0;
    }
    if (grayCounter == beatCount) {
        grayCounter = 0;
    }
    NSLog(@"beatCount = %d +++++ counter = %d +++++ grayCounter = %d",beatCount,counter,grayCounter);
    
    UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:grayCounter];
    [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
    
    UIImageView *beatImg = (UIImageView*)[self.beatsView viewWithTag:counter];
    [beatImg setImage:[UIImage imageNamed:@"beat_ball_green.png"]];
    
    counter++;
    grayCounter++;
}

- (IBAction)onTapFringe:(id)sender {
    
    NSURL *videoURL;
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSArray *filePathsArray = [[NSFileManager defaultManager] subpathsOfDirectoryAtPath:documentsDirectory  error:nil];
    NSLog(@"files array %@", filePathsArray);
    
    NSString *fullpath;
    
    
    fullpath = [documentsDirectory stringByAppendingPathComponent:[filePathsArray objectAtIndex:[filePathsArray count]-1]];
    videoURL =[NSURL fileURLWithPath:fullpath];
    
    NSLog(@"starting %@",videoURL);
    myplayer = [[AVAudioPlayer alloc]initWithContentsOfURL:videoURL error:nil];
    //  myplayer.numberOfLoops = -1;
    myplayer.enableRate = true;
    [myplayer play];
    /*
    FrequencyViewController *productDetailsView =[self.storyboard instantiateViewControllerWithIdentifier:@"frequencyVC"];
    [self presentViewController:productDetailsView animated:YES completion:nil];
    */
}

- (void)playMemos:(NSString *)memo
{
    _audioPlayerClap4 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@.wav", [[NSBundle mainBundle] resourcePath], memo]] error:nil];
    _audioPlayerClap4.volume = 1.0f;
    _audioPlayerClap4.numberOfLoops = -1;
}

- (void) playRhythmsWithBeats:(AVAudioPlayer*)audioPlayer:(int)beatNumber
{
    audioPlayer.numberOfLoops = -1;
    audioPlayer.enableRate = true;
    audioPlayer.rate = mCurrentScore/tempo;
    [audioPlayer play];
}

-(void)playSavedMusic{
    
    
    
    //    MPMoviePlayerViewController *videoPlayerView = [[MPMoviePlayerViewController alloc] initWithContentURL:videoURL];
//    [self presentMoviePlayerViewControllerAnimated:videoPlayerView];
//    [videoPlayerView.moviePlayer play];
    
//    NSError *error;
//    AVAudioPlayer *myplayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[[NSURL alloc] initFileURLWithPath:newPath] error:&error];
//    NSLog(@"Error = %@",error);
//    NSLog(@"newpath = %@",newPath);
//    
//    myplayer.numberOfLoops = -1;
//    myplayer.enableRate = true;
//    myplayer.currentTime = 0;
//    [myplayer play];
}

- (void) playSelectedRhythms:(NSString*)playRythm
{
    
    currentRythmName = [countArray objectAtIndex:[_carousel currentItemIndex]];
   // NSLog(@"beats1 = %@",[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], beatOneMusicFile]);
 //   NSLog(@"Beat2 = %@",[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], beatTwoMusicFile ]);
// NSLog(@"beat 3 = %@",[NSString stringWithFormat:@"%@/Click AccentedNew.wav", [[NSBundle mainBundle] resourcePath]]);
  //  NSLog(@"Current label = %@",currentRythmName);
    if(clapFlag1 == 1) {
        NSArray *listItems = [beatOneMusicFile componentsSeparatedByString:@"/"];
        NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
        
        
        _audioPlayerClap1 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString]] error:nil];
        
        [self playRhythmsWithBeats:_audioPlayerClap1 :0];
    }
    
    if(clapFlag2 == 1) {
        NSArray *listItems = [beatOneMusicFile componentsSeparatedByString:@"/"];
        NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
        
        _audioPlayerClap2 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString ]] error:nil];
        [self playRhythmsWithBeats:_audioPlayerClap2 :0];
    }
    
    if(clapFlag3 == 1) {
        _audioPlayerClap3 = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/Click AccentedNew.wav", [[NSBundle mainBundle] resourcePath]]] error:nil];
        //_audioPlayerClap3.rate = mCurrentScore/60.0;
        [self playRhythmsWithBeats:_audioPlayerClap3 :0];
    }
    
    if(clapFlag4 == 1) {
        [self playMemos:_droneType];
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 play];
    }
    
    
    if(clapFlag1 == 0 && clapFlag2 == 0 && clapFlag3 == 0) {
        //[self playSongCall:1];
    }
}

- (void) stopSelectedRhythms
{
    if(clapFlag1 == 1) {
        _audioPlayerClap1.currentTime = 0;
        [_audioPlayerClap1 stop];
    }
    
    if(clapFlag2 == 1) {
        _audioPlayerClap2.currentTime = 0;
        [_audioPlayerClap2 stop];
    }
    
    if(clapFlag3 == 1) {
        _audioPlayerClap3.currentTime = 0;
        [_audioPlayerClap3 stop];
    }
    
    if(clapFlag4 == 1) {
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 stop];
    }
    
    if(_audioPlayer.isPlaying) {
        _audioPlayer.currentTime = 0;
        [_audioPlayer stop];
    }
}

#pragma mark - UIPickerView Datasource mehods

- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [dronePickerArray count];
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return dronePickerArray[row];
}
-(UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(5, 0, 50, 27)];
    [lbl setBackgroundColor:[UIColor clearColor]];
    lbl.textColor = [UIColor whiteColor];
    [lbl setFont:[UIFont fontWithName:@"HelveticaNeue" size:25.0]];
    
    lbl.text = dronePickerArray[row];
    
    [lbl setTextAlignment:NSTextAlignmentCenter];
    return lbl;
    
    //    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(5, 0, 50, 27)];
    //    [btn addTarget:self action:@selector(handleSwipeFrom) forControlEvents:UIControlEventTouchDragInside];
    //    [btn setTitle:dronePickerArray[row] forState:UIControlStateNormal];
    //    btn.backgroundColor = [UIColor redColor];
    //
    //    return btn;
}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    return 60;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    NSLog(@"row = %@ ",[dronePickerArray objectAtIndex:[pickerView selectedRowInComponent:component]]);
    _droneType = [dronePickerArray objectAtIndex:[pickerView selectedRowInComponent:component]];
    if (playFlag == 1) {
        [self playMemos:_droneType];
        _audioPlayerClap4.currentTime = 0;
        [_audioPlayerClap4 play];
    }
}

#pragma mark- iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [countArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 220.0f, 80.0f)];
        // ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        label.font = [label.font fontWithSize:25];
        label.tag = 1;
       // label.text = [countArray objectAtIndex:index];
        [view addSubview:label];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [countArray objectAtIndex:index];
    return view;
}

-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer{
    // return
    return true;
}


- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    if (option == iCarouselOptionSpacing)
    {
        return value * 1.5;
    }
    return value;
}

// to stop sound
- (void)carouselCurrentItemIndexDidChange:(iCarousel *)carousel{
   
    [_stopBtn setEnabled:YES];
    [_playBtn setBackgroundImage:[UIImage imageNamed:@"play-button.png"] forState:UIControlStateNormal];
    
    [_playTimerBtn setHidden:YES];
    [_playStopBtn setHidden:YES];
    
    [self stopSelectedRhythms];
    
    [_playTimer invalidate];
    _playTimer = nil;
    
    [beatTimer invalidate];
    beatTimer = nil;
    for (int i = 0; i < 12; i++) {
        UIImageView *grayBeatImg = (UIImageView*)[self.beatsView viewWithTag:i];
        [grayBeatImg setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
    }
    
    seconds = 0;
    minutes = 0;
    
    playFlag = 0;
    NSLog(@"Index ------ %ld",(long)[_carousel currentItemIndex]);
    
    //  Never Never
    if ([_carousel currentItemIndex] < 0) {
        _carousel.currentItemIndex = 0;
    }
    if ([countArray count] != 0) {
        [self setDataToUIElements:(int)[_carousel currentItemIndex]];
        caraouselIndex = (int)[_carousel currentItemIndex];
        
        // Default rythm sent to player
        currentRythmName = [countArray objectAtIndex:caraouselIndex];
        float value = [self bpmForSelectedRythm:[countArray objectAtIndex:caraouselIndex]];
        _bpmString = [NSString stringWithFormat:@"%d bpm", (int)value];
        [_bpmTxt setText:_bpmString];
        mCurrentScore = value;
        [_bpmSlider setValue:mCurrentScore];
    }    
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel{
    
}

// Set UI elements on Caraousel Item Change
-(void)setDataToUIElements:(int)_index{
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    RhythmClass *selectedRhythmClass = [[RhythmClass alloc]init];
    if ([countArray count] != 0) {
        
        currentRythmName = [countArray objectAtIndex:_index];
        for (RhythmClass *cls in rhythmArray) {
            if ([[cls valueForKey:@"rhythmName"] isEqualToString:currentRythmName]) {
                appDelegate.latestRhythmClass = cls;
       //         selectedRhythmClass = cls;
                break;
            }
        }
    }
    selectedRhythmClass = appDelegate.latestRhythmClass;
    
    // Set Music
    beatOneMusicFile = selectedRhythmClass.rhythmBeatOne;
    beatTwoMusicFile = selectedRhythmClass.rhythmBeatTwo;
    
    // Set Image
    if (![selectedRhythmClass.rhythmInstOneImage isEqualToString:@"-1"]) {
        [_instBtn1 setImage:[UIImage imageNamed:selectedRhythmClass.rhythmInstOneImage] forState:UIControlStateNormal];
        [_instBtn1 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",selectedRhythmClass.rhythmInstOneImage]] forState:UIControlStateSelected];
    }
    if (![selectedRhythmClass.rhythmInstTwoImage isEqualToString:@"-1"]) {
        [_instBtn2 setImage:[UIImage imageNamed:selectedRhythmClass.rhythmInstTwoImage] forState:UIControlStateNormal];
        [_instBtn2 setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%@_disabled",selectedRhythmClass.rhythmInstTwoImage]] forState:UIControlStateSelected];
    }
    
    // Set BPM values
    if ([countArray count] != 0) {
        
        
        float value = [self bpmForSelectedRythm:[countArray objectAtIndex:[_carousel currentItemIndex]]];
        _bpmString = [NSString stringWithFormat:@"%d bpm", (int)value];
        [_bpmTxt setText:_bpmString];
        
        mCurrentScore = [selectedRhythmClass.rhythmStartBPM intValue];
        tempo = value;
        
        [_bpmSlider setValue:value animated:YES];
        _audioPlayerClap1.rate = mCurrentScore/tempo;
        _audioPlayerClap2.rate = mCurrentScore/tempo;
        
    }
    
    // Set Beats Meter
    
    beatCount = [selectedRhythmClass.rhythmBeatsCount intValue];
    if (beatCount != 0) {
        
        // [[UIScreen mainScreen] bounds].size.width
        float XDist = (((320) - (beatCount*21))/(beatCount+1));
        NSLog(@"Beat Count = %d === XDist = %f",beatCount,XDist);
        
        float temp = 0.0;
        
        for (int i = 0; i < beatCount; i++) {
            UIImageView *img = (UIImageView*)[self.beatsView viewWithTag:i];
          //  [img setImage:[UIImage imageNamed:@"beat_ball_green.png"]];
         //   NSLog(@"img tag ========= %d",img.tag);
            temp = lroundf((i *21) + ((i+1)*XDist));
            img.frame = CGRectMake(temp, 0, 21, 21);
            NSLog(@"temp = %f",temp);
           // [self.beatsView addSubview:img];
        }
        for (int j = beatCount; j <12; j++) {
            UIImageView *img1 = (UIImageView*)[self.beatsView viewWithTag:j];
            NSLog(@"the image tag: %d",j);
        //    [img1 setImage:[UIImage imageNamed:@"beat_ball_grey.png"]];
            img1.frame = CGRectMake(9000, 3, 21, 21);
          //  [img1 removeFromSuperview];
        }
    }
    
}
// To learn about notifications, see "Notifications" in Cocoa Fundamentals Guide.
- (void) registerForMediaPlayerNotifications {
    
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_NowPlayingItemChanged:)
                               name: MPMusicPlayerControllerNowPlayingItemDidChangeNotification
                             object: _musicPlayer];
    
    [notificationCenter addObserver: self
                           selector: @selector (handle_PlaybackStateChanged:)
                               name: MPMusicPlayerControllerPlaybackStateDidChangeNotification
                             object: _musicPlayer];
    
    /*
     // This sample doesn't use libray change notifications; this code is here to show how
     //		it's done if you need it.
     [notificationCenter addObserver: self
     selector: @selector (handle_iPodLibraryChanged:)
     name: MPMediaLibraryDidChangeNotification
     object: musicPlayer];
     
     [[MPMediaLibrary defaultMediaLibrary] beginGeneratingLibraryChangeNotifications];
     */
    
    [_musicPlayer beginGeneratingPlaybackNotifications];
}

#pragma mark Music notification handlers__________________

// When the now-playing item changes, update the media item artwork and the now-playing label.
- (void) handle_NowPlayingItemChanged: (id) notification {
    
    MPMediaItem *currentItem = [_musicPlayer nowPlayingItem];
    
    // Assume that there is no artwork for the media item.
    UIImage *artworkImage = _noArtworkImage;
    
    // Get the artwork from the current media item, if it has artwork.
    MPMediaItemArtwork *artwork = [currentItem valueForProperty: MPMediaItemPropertyArtwork];
    
    // Obtain a UIImage object from the MPMediaItemArtwork object
    if (artwork) {
        artworkImage = [artwork imageWithSize: CGSizeMake (30, 30)];
    }
    
    // Obtain a UIButton object and set its background to the UIImage object
    UIButton *artworkView = [[UIButton alloc] initWithFrame: CGRectMake (0, 0, 30, 30)];
    [artworkView setBackgroundImage: artworkImage forState: UIControlStateNormal];
    
    // Obtain a UIBarButtonItem object and initialize it with the UIButton object
    UIBarButtonItem *newArtworkItem = [[UIBarButtonItem alloc] initWithCustomView: artworkView];
    [self setArtworkItem: newArtworkItem];
//    [newArtworkItem release];
    
    [_artworkItem setEnabled: NO];
    
    // Display the new media item artwork
  //  [navigationBar.topItem setRightBarButtonItem: artworkItem animated: YES];
    
    // Display the artist and song name for the now-playing media item
    [_nowPlayingLabel setText: [
                               NSString stringWithFormat: @"%@ %@ %@ %@",
                               NSLocalizedString (@"Now Playing:", @"Label for introducing the now-playing song title and artist"),
                               [currentItem valueForProperty: MPMediaItemPropertyTitle],
                               NSLocalizedString (@"by", @"Article between song name and artist name"),
                               [currentItem valueForProperty: MPMediaItemPropertyArtist]]];
    
    if (_musicPlayer.playbackState == MPMusicPlaybackStateStopped) {
        // Provide a suitable prompt to the user now that their chosen music has
        //		finished playing.
        [_nowPlayingLabel setText: [
                                   NSString stringWithFormat: @"%@",
                                   NSLocalizedString (@"Music-ended Instructions", @"Label for prompting user to play music again after it has stopped")]];
        
    }
}

// When the playback state changes, set the play/pause button in the Navigation bar
//		appropriately.
- (void) handle_PlaybackStateChanged: (id) notification {
    
    MPMusicPlaybackState playbackState = [_musicPlayer playbackState];
    
    if (playbackState == MPMusicPlaybackStatePaused) {
        
      //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
    } else if (playbackState == MPMusicPlaybackStatePlaying) {
        
      //  navigationBar.topItem.leftBarButtonItem = pauseBarButton;
        
    } else if (playbackState == MPMusicPlaybackStateStopped) {
        
      //  navigationBar.topItem.leftBarButtonItem = playBarButton;
        
        // Even though stopped, invoking 'stop' ensures that the music player will play  
        //		its queue from the start.
        [_musicPlayer stop];
        
    }
}

/*
- (void) setupApplicationAudio {
    
    // Gets the file system path to the sound to play.
    NSString *soundFilePath = [[NSBundle mainBundle]	pathForResource:	@"sound"
                                                              ofType:				@"caf"];
    
//    // Converts the sound's file path to an NSURL object
//    NSURL *newURL = [[NSURL alloc] initFileURLWithPath: soundFilePath];
//    self.soundFileURL = newURL;
//    [newURL release];
    
    // Registers this class as the delegate of the audio session.
    [[AVAudioSession sharedInstance] setDelegate: self];
    
    // The AmbientSound category allows application audio to mix with Media Player
    // audio. The category also indicates that application audio should stop playing
    // if the Ring/Siilent switch is set to "silent" or the screen locks.
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryAmbient error: nil];
 
     // Use this code instead to allow the app sound to continue to play when the screen is locked.
     [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error: nil];
     
     UInt32 doSetProperty = 0;
     AudioSessionSetProperty (
     kAudioSessionProperty_OverrideCategoryMixWithOthers,
     sizeof (doSetProperty),
     &doSetProperty
     );
     */
    /*
    // Registers the audio route change listener callback function
    AudioSessionAddPropertyListener (
                                     kAudioSessionProperty_AudioRouteChange,
                                     audioRouteChangeListenerCallback,
                                     self
                                     );
    
    // Activates the audio session.
    
    NSError *activationError = nil;
    [[AVAudioSession sharedInstance] setActive: YES error: &activationError];
    
//    // Instantiates the AVAudioPlayer object, initializing it with the sound
//    AVAudioPlayer *newPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL: soundFileURL error: nil];
//    self.appSoundPlayer = newPlayer;
//    [newPlayer release];
//    
//    // "Preparing to play" attaches to the audio hardware and ensures that playback
//    //		starts quickly when the user taps Play
//    [appSoundPlayer prepareToPlay];
//    [appSoundPlayer setVolume: 1.0];
//    [appSoundPlayer setDelegate: self];
}

*/
@end
