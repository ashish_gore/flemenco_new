//
//  MainNavigationViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "MainNavigationViewController.h"
#import "ViewController.h"
#import "RecordViewController.h"
#import "SavedListViewController.h"



@interface MainNavigationViewController () {
    UIScrollView *pageScrollView;
    NSInteger currentPageIndex;
    SavedListViewController *thirdVC;
    RecordViewController *secondVC;
}

@end

@implementation MainNavigationViewController

//@synthesize viewControllerArray;
//@synthesize selectionBar;
//@synthesize panGestureRecognizer;
@synthesize pageController;
//@synthesize navigationView;
//@synthesize buttonText;


- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.self.navigationBar.barTintColor = [UIColor colorWithRed:0.01 green:0.05 blue:0.06 alpha:1]; //%%% bartint
    //self.navigationBar.translucent = NO;
    /*
    viewControllerArray = [[NSMutableArray alloc]init];
    currentPageIndex = 0;
     */
    
    self.pageController = [self.storyboard instantiateViewControllerWithIdentifier:@"PageViewController"];
    self.pageController.dataSource = self;
    
    ViewController *firstVC = [self.storyboard instantiateViewControllerWithIdentifier:@"firstVC"];
    secondVC= [self.storyboard instantiateViewControllerWithIdentifier:@"secondVC"];
    thirdVC = [self.storyboard instantiateViewControllerWithIdentifier:@"thirdVCold"];
    
    firstVC.myNavigationController = self;
    secondVC.myNavigationController = self;
    
    viewControllerArray = @[firstVC,secondVC,thirdVC];
    
    //    ProductDetailViewController *startingViewController = [self viewControllerAtIndex:0];
    NSArray *viewControllers = @[firstVC];
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:NO completion:nil];
    
    // Change the size of page view controller
    self.pageController.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height );
    
    [self.view addSubview:pageController.view];
    [self addChildViewController:pageController];
    [self.pageController didMoveToParentViewController:self];
    
}
//This stuff here is customizeable: buttons, views, etc
////////////////////////////////////////////////////////////
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
//%%%%%%%%%%%%%%%%%    CUSTOMIZEABLE    %%%%%%%%%%%%%%%%%%//
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%//
//

//%%% color of the status bar
-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
    
    //    return UIStatusBarStyleDefault;
}

-(void)viewToPresent:(int)_index withDictionary:(NSDictionary*)_dict{
    
    NSArray *viewControllers = @[[viewControllerArray objectAtIndex:_index]];
    // For third View Controller
    if (_index == 1) {
        secondVC.genreIdDict = _dict;
        secondVC.bpmDefaultFlag = 1;
    }
    if (_index == 2) {
//        thirdVC.musicFileDict = _dict;
//        [thirdVC addSongToTableView];
    }
    [self.pageController setViewControllers:viewControllers direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:nil];
//    if (_index == 1) {
//        [secondVC setDataToUIElements:0];
//    }
}

//-(void)initialViewToPresent:(int)_index withDictionary:(NSDictionary*)_dict{
//    
//    // For third View Controller
//    if (_index == 1) {
//        secondVC.genreIdDict = _dict;
//    }
//}

-(void)updateCurrentPageIndex:(int)newIndex
{
    currentPageIndex = newIndex;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfController:viewController];;
    
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
//    if (index == 2) {
//        [secondVC setDataToUIElements:0];
//    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfController:viewController];;
    
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == 3) {
        return nil;
    }
    if (index == 2) {
        secondVC.bpmDefaultFlag = 0;
       // [secondVC setDataToUIElements:0];
    }
    return [self viewControllerAtIndex:index];
}

- (UIViewController *)viewControllerAtIndex:(NSUInteger)index
{
    if ((index == -1) || (index >= 3)) {
        return nil;
    }
    
    return [viewControllerArray objectAtIndex:index];;
}

-(NSInteger)indexOfController:(UIViewController *)viewController
{
    for (int i = 0; i<[viewControllerArray count]; i++) {
        if (viewController == [viewControllerArray objectAtIndex:i])
        {
            return i;
        }
    }
    return NSNotFound;
}
@end