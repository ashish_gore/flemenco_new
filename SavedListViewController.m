//
//  SavedListViewController.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "SavedListViewController.h"
#import "protypeTableCell.h"
#import "DBManager.h"
#import "RecordingListData.h"

#define NUM_TOP_ITEMS 20
#define NUM_SUBITEMS 6

@implementation SavedListViewController

- (id)init {
    self = [super init];
    
    if (self) {
        
    }
    return self;
}

#pragma mark - methods

- (void)setCellBackGroud :(protypeTableCell *)cell setColor:(UIColor *)color
{
    UIView * selectedBackgroundView = [[UIView alloc] initWithFrame:cell.backgroundView.frame];
    [selectedBackgroundView setBackgroundColor:color]; // set color here
    [cell setSelectedBackgroundView:selectedBackgroundView];
    if (color == [UIColor whiteColor]) {
                cell.songNameLbl.textColor = [UIColor blackColor];
                cell.dateLbl.textColor = [UIColor blackColor];
                cell.TotalTimeLbl.textColor = [UIColor blackColor];
                cell.songDetailLbl.textColor = [UIColor blackColor];
                cell.closeBtn.hidden = NO;
    }
    else
    {
        cell.songNameLbl.textColor = [UIColor whiteColor];
        cell.dateLbl.textColor = [UIColor whiteColor];
        cell.TotalTimeLbl.textColor = [UIColor whiteColor];
        cell.songDetailLbl.textColor = [UIColor whiteColor];
        cell.closeBtn.hidden = YES;
    }
    
    //[cell.backgroundView removeFromSuperview];
}

-(void)resetAll
{
    buttonCount = 0;
    recordButtonEnabled = NO;
}


-(void)unhideObjectFromCell :(BOOL)unHide
{
    
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentExpandedIndex +1 inSection:0];
    protypeTableCell *buttonCell = (protypeTableCell *) [self.songTableView cellForRowAtIndexPath:index];
    [self.songTableView beginUpdates];// first handle row hieght events automatically
    [self.songTableView endUpdates];
    if(unHide)
    
        [UIView animateWithDuration:10
                              delay:10
                            options: UIViewAnimationOptionBeginFromCurrentState
                         animations:^
         {
            [buttonCell.recorderView setHidden:NO];// then unhide the button
         }
                         completion:^(BOOL finished)
         {
         }];
    
    
    else
        [UIView animateWithDuration:10
                              delay:10
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:^
         {
             [buttonCell.recorderView setHidden:YES];// then hide the button
         }
                         completion:^(BOOL finished)
         {
         }];
    
}


- (void)editBtnClicked:(UIButton*)sender
{
    dragEnabled = YES;
}

-(void)closeButtonClicked:(UIButton*)sender
{
    parentSelectedCount = 0;
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentExpandedIndex inSection:0];
    protypeTableCell *Cell = (protypeTableCell *) [self.songTableView cellForRowAtIndexPath:index];
    NSArray *indexArray = [[NSArray alloc] initWithObjects:index, nil];
    
    Cell.closeBtn.hidden = YES;
    Cell.tapCount = 0;
    [self.songTableView beginUpdates];
    [self collapseSubItemsAtIndex:currentExpandedIndex];
    currentExpandedIndex = -1;
    
    [self.songTableView endUpdates];
   
    [_songTableView reloadRowsAtIndexPaths:indexArray withRowAnimation:UITableViewRowAnimationNone];
    //[_songTableView reloadData];
    
}
-(void)recordButtonClicked:(UIButton*)sender
{
    buttonCount ++;
    if (buttonCount > 1) {
        buttonCount = 0;
        increaseChildRowHieght = NO;
        recordButtonEnabled = NO;
        [self unhideObjectFromCell:increaseChildRowHieght];
    }
    else
    {
        increaseChildRowHieght = YES;
        recordButtonEnabled = YES;
        [self unhideObjectFromCell :increaseChildRowHieght];
    }
}

-(void)playButtonClicked:(UIButton*)sender
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentExpandedIndex inSection:0];
    NSIndexPath *childIndex = [NSIndexPath indexPathForRow:currentExpandedIndex +1 inSection:0];
    protypeTableCell *buttonCell = (protypeTableCell *) [self.songTableView cellForRowAtIndexPath:childIndex];
    RecordingListData *cellData = [[RecordingListData alloc] init];
    cellData = [songList objectAtIndex:index.row];
    if (playBtnCount == 0) {
    [self playSelectedRecording:cellData.trackOne];
    playBtnCount = 1;
    }else
    {
        [_recAudioPlayer stop];
         _recAudioPlayer = nil;
         playBtnCount = 0;
    }


}


- (void) playSelectedRecording:(NSString*)playRec
{
    
    //currentRythmName = [countArray objectAtIndex:[_carousel currentItemIndex]];
    // NSLog(@"beats1 = %@",[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], beatOneMusicFile]);
    //   NSLog(@"Beat2 = %@",[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], beatTwoMusicFile ]);
    // NSLog(@"beat 3 = %@",[NSString stringWithFormat:@"%@/Click AccentedNew.wav", [[NSBundle mainBundle] resourcePath]]);
    //  NSLog(@"Current label = %@",currentRythmName);
    
        NSArray *listItems = [playRec componentsSeparatedByString:@"/"];
        NSString *lastWordString = [NSString stringWithFormat:@"%@", listItems.lastObject];
        
//        
//        _recAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/%@", [[NSBundle mainBundle] resourcePath], lastWordString]] error:nil];
    _recAudioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:playRec] error:nil];
        
        [self playRecordingWithBeats:_recAudioPlayer :0];
    
}

- (void) playRecordingWithBeats:(AVAudioPlayer*)audioPlayer:(int)beatNumber
{
        audioPlayer.numberOfLoops = -1;
        audioPlayer.enableRate = true;
        //audioPlayer.rate = mCurrentScore/tempo;
        [audioPlayer play];
}


- (IBAction) imageMoved:(id) sender withEvent:(UIEvent *) event
{
    //BOOL insideTrash = NO;
    
    if (dragEnabled)
    {
    UIControl *control = sender ;
    int tag = (int)control.tag;
    NSLog(@"tag = %d",tag);
    UITouch *t = [[event allTouches] anyObject];
    CGPoint pPrev = [t previousLocationInView:control];
    CGPoint p = [t locationInView:control];
    
    CGPoint center = control.center;
    center.x += p.x - pPrev.x;
    center.y += p.y - pPrev.y;
    control.center = center;
    
    
    if(CGRectContainsPoint(control.frame, trashButton.center))
       {
           
           //control.center = forthKnobCentre;
           
           if(t.phase == UITouchPhaseEnded)
           {
//               control.hidden = YES;
//               control.center = forthKnobCentre;
//               secondKnob.center = control.center;
//               thirdKnob.center = control.center;
//               forthKnob.center = control.center;
               
               if(tag == 1)
               {
               control.hidden = YES;
               control.center = secondKnobCentre;
               secondKnob.center = thirdKnobCentre;
               thirdKnob.center = forthKnobCentre;
               forthKnob.center = forthKnobCentre;
               
               
               [UIView animateWithDuration:0.5
                                     delay:0
                                   options:UIViewAnimationCurveEaseOut
                                animations:^{
                                    // set the new frame
                                    control.hidden = NO;
                                    control.center = firstKnobCentre;
                                    secondKnob.center = secondKnobCentre;
                                    thirdKnob.center = thirdKnobCentre;
                                    forthKnob.hidden = YES;
                                    //control.center = forthKnobCentre;
                                    
                                }
                                completion:^(BOOL finished){
                                    NSLog(@"Done!");
                                    //sleep(1);
                                    forthKnob.hidden = NO;
                                    forthKnob.center = forthKnobCentre;
                                }
                ];
               }
               
               if(tag == 2)
               {
                   control.hidden = YES;
                   control.center = thirdKnobCentre;
                   //secondKnob.center = thirdKnobCentre;
                   thirdKnob.center = forthKnobCentre;
                   forthKnob.center = forthKnobCentre;
                   
                   
                   [UIView animateWithDuration:0.5
                                         delay:0
                                       options:UIViewAnimationCurveEaseOut
                                    animations:^{
                                        // set the new frame
                                        control.hidden = NO;
                                        control.center = secondKnobCentre;
                                        //secondKnob.center = secondKnobCentre;
                                        thirdKnob.center = thirdKnobCentre;
                                        forthKnob.hidden = YES;
                                        //control.center = forthKnobCentre;
                                        
                                    }
                                    completion:^(BOOL finished){
                                        NSLog(@"Done!");
                                        //sleep(1);
                                        forthKnob.hidden = NO;
                                        forthKnob.center = forthKnobCentre;
                                    }
                    ];
               }
               
               if(tag == 3)
               {
                   control.hidden = YES;
                   control.center = forthKnobCentre;
                   //secondKnob.center = thirdKnobCentre;
                   //thirdKnob.center = forthKnobCentre;
                   forthKnob.center = forthKnobCentre;
                   
                   
                   [UIView animateWithDuration:0.5
                                         delay:0
                                       options:UIViewAnimationCurveEaseOut
                                    animations:^{
                                        // set the new frame
                                        control.hidden = NO;
                                        control.center = thirdKnobCentre;
                                        //secondKnob.center = secondKnobCentre;
                                        //thirdKnob.center = thirdKnobCentre;
                                        forthKnob.hidden = YES;
                                        //control.center = forthKnobCentre;
                                        
                                    }
                                    completion:^(BOOL finished){
                                        NSLog(@"Done!");
                                        //sleep(1);
                                        forthKnob.hidden = NO;
                                        forthKnob.center = forthKnobCentre;
                                    }
                    ];
               }
               if(tag == 4)
               {
                   control.hidden = YES;
                   control.center = forthKnobCentre;
                   //secondKnob.center = thirdKnobCentre;
                   //thirdKnob.center = forthKnobCentre;
                   forthKnob.center = forthKnobCentre;
                   
                   
                   [UIView animateWithDuration:0.5
                                         delay:0
                                       options:UIViewAnimationCurveEaseOut
                                    animations:^{
                                        // set the new frame
                                        control.hidden = NO;
                                        //control.center = forthKnobCentre;
                                        //secondKnob.center = secondKnobCentre;
                                        //thirdKnob.center = thirdKnobCentre;
                                        //forthKnob.hidden = YES;
                                        //control.center = forthKnobCentre;
                                        
                                    }
                                    completion:^(BOOL finished){
                                        NSLog(@"Done!");
                                        //sleep(1);
//                                        forthKnob.hidden = NO;
//                                        forthKnob.center = forthKnobCentre;
                                    }
                    ];
               }
               
           }
       }
    else
    {
        if(t.phase == UITouchPhaseEnded)
        {
            if (control.tag == 1) {
                control.hidden = NO;
                control.center = firstKnobCentre;
            }
            if (control.tag == 2) {
                    control.hidden = NO;
                    control.center = secondKnobCentre;
                }
            if (control.tag == 3) {
                control.hidden = NO;
                control.center = thirdKnobCentre;
            }
            if (control.tag == 4) {
                control.hidden = NO;
                control.center = forthKnobCentre;
            }
            
        }
      }
       // dragEnabled = NO;
    }
}


#pragma mark - Data generators

- (NSArray *)topLevelItems {
    NSMutableArray *items = [NSMutableArray array];
    
    for (int i = 0; i < NUM_TOP_ITEMS; i++) {
        [items addObject:[NSString stringWithFormat:@"Item %d", i + 1]];
    }
    
    return items;
}

- (NSArray *)subItems {
    NSMutableArray *items = [NSMutableArray array];
    //int numItems = arc4random() % NUM_SUBITEMS + 2;
    int numItems = 1;
    
    for (int i = 0; i < numItems; i++) {
        [items addObject:[NSString stringWithFormat:@"SubItem %d", i + 1]];
    }
    
    return items;
}

#pragma mark - View management


-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_recAudioPlayer stop];
    _recAudioPlayer = nil;
}


-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // test commit
    
    
//    songList = [[NSMutableArray alloc] initWithObjects:@"Tangos (100 BPM A#)",@"My Second Song",@"My Third Song",@"My Forth Song",@"Thriller Remix",@"MC Hammer"@"Save The World",@"Get Down",@"Get Up",@"Good Morning", nil];
        //
    DBManager *sqlManager = [[DBManager alloc] init];
    NSMutableArray *dataArray = [[NSMutableArray alloc] init];
    dataArray = [sqlManager getAllRecordingData];
    songList = [[NSMutableArray alloc] init];
    songList = dataArray;
    
    subItems = [NSMutableArray new];
    for (int i = 0; i < [songList count]; i++) {
        [subItems addObject:[self subItems]];
    }
    
    [_songTableView reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    increaseChildRowHieght = NO;
    recordButtonEnabled = NO;
    buttonCount = 0;
    
    

    // topItems = [[NSArray alloc] initWithArray:[self topLevelItems]];
    
    currentExpandedIndex = -1;
    
    
    [_volumeSlider setThumbImage:[UIImage imageNamed:@"slider-thumb.png"] forState:UIControlStateNormal];
    [_volumeSlider setMaximumTrackImage:[UIImage imageNamed:@"slidertrackLatestBlack.png"] forState:UIControlStateNormal];
    [_volumeSlider setMinimumTrackImage:[UIImage imageNamed:@"sliderprogressLatest.png"] forState:UIControlStateNormal];
    
    
}

//-(void)addSongToTableView{
//    [songList insertObject:[_musicFileDict valueForKey:@"name"] atIndex:0];
//    [_songTableView reloadData];
//}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [songList count] + ((currentExpandedIndex > -1) ? [[subItems objectAtIndex:currentExpandedIndex] count] : 0);
    //return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    RecordingListData *cellData = [[RecordingListData alloc] init];
    static NSString *ParentCellIdentifier = @"songCell";
    //static NSString *ParentCellIdentifier = @"songCell";
    static NSString *ChildCellIdentifier = @"childCell";
    
    BOOL isChild =
    currentExpandedIndex > -1
    && indexPath.row > currentExpandedIndex
    && indexPath.row <= currentExpandedIndex + [[subItems objectAtIndex:currentExpandedIndex] count];
    
    //UITableViewCell *cell;
    protypeTableCell *cell;
    
    if (isChild) {
        cell = [tableView dequeueReusableCellWithIdentifier:ChildCellIdentifier];
    }
    else {
        cell = [tableView dequeueReusableCellWithIdentifier:ParentCellIdentifier];
    }
    
    
    if (cell == nil) {
        cell = [[protypeTableCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:ParentCellIdentifier];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    
    if (isChild) {
        //cell.detailTextLabel.text = [[subItems objectAtIndex:currentExpandedIndex] objectAtIndex:indexPath.row - currentExpandedIndex - 1];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        trashButton = cell.trashDefaultBtn;
        firstKnob = cell.firstVolumeKnob;
        secondKnob = cell.secondVolumeKnob;
        thirdKnob = cell.thirdVolumeKnob;
        forthKnob = cell.fourthVolumeKnob;
        
        firstKnobCentre = firstKnob.center;
        secondKnobCentre = secondKnob.center;
        thirdKnobCentre = thirdKnob.center;
        forthKnobCentre = forthKnob.center;
        
        if(!recordButtonEnabled)
         cell.recorderView.hidden = YES;  // have to hide to reset hiding with every selection of cell
        //cell.closeBtn.hidden = NO;
        //[cell.childCellBtn addTarget:self action:@selector(yourButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.trashDefaultBtn addTarget:self action:@selector(imageMoved:withEvent:) forControlEvents:UIControlEventTouchDown];
        [cell.firstVolumeKnob addTarget:self action:@selector(imageMoved:withEvent:) forControlEvents:UIControlEventAllTouchEvents];
        [cell.secondVolumeKnob addTarget:self action:@selector(imageMoved:withEvent:) forControlEvents:UIControlEventAllTouchEvents];
        [cell.thirdVolumeKnob addTarget:self action:@selector(imageMoved:withEvent:) forControlEvents:UIControlEventAllTouchEvents];
        [cell.fourthVolumeKnob addTarget:self action:@selector(imageMoved:withEvent:) forControlEvents:UIControlEventAllTouchEvents];
        [cell.fourthVolumeKnob addTarget:self action:@selector(recordButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.editBtn addTarget:self action:@selector(editBtnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [cell.playRecBtn addTarget:self action:@selector(playButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
       
        // http://stackoverflow.com/questions/19225475/uibutton-with-hold-down-action-and-release-action
        //http://stackoverflow.com/questions/4707858/basic-drag-and-drop-in-ios
    }
    else {
//        NSIndexPath* newIndexPath = [NSIndexPath indexPathForRow:previousIndexPath.row-1 inSection:previousIndexPath.section];
//       [tableView deselectRowAtIndexPath:indexPath animated:YES];
        [cell.closeBtn addTarget:self action:@selector(closeButtonClicked:) forControlEvents:UIControlEventTouchUpInside];
        int topIndex = (currentExpandedIndex > -1 && indexPath.row > currentExpandedIndex)
        ? indexPath.row - [[subItems objectAtIndex:currentExpandedIndex] count]
        : indexPath.row;
        cell.closeBtn.hidden = YES;
        //cell.songNameLbl.text = [songList objectAtIndex:topIndex];
        cellData = [songList objectAtIndex:topIndex];
        cell.songNameLbl.text = cellData.recordingName;
        //cell.textLabel.text = [topItems objectAtIndex:topIndex];
        //cell.detailTextLabel.text = @"";
    }
    
    return cell;
    
}

#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView
heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL isChild =
    currentExpandedIndex > -1
    && indexPath.row > currentExpandedIndex
    && indexPath.row <= currentExpandedIndex + [[subItems objectAtIndex:currentExpandedIndex] count];
    //protypeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChildCell"];
    if (isChild) {
        //NSLog(@"A child was tapped, do what you will with it");
        
        if(increaseChildRowHieght)
        {
            increaseChildRowHieght = NO;
            return 378;
        }
        else
        {
            
            return 160;
        }
    }
    else
    {
        
        return 60;
    }
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self resetAll];
    
    //[tableView deselectRowAtIndexPath:indexPath animated:YES];
    BOOL isChild =
    currentExpandedIndex > -1
    && indexPath.row > currentExpandedIndex
    && indexPath.row <= currentExpandedIndex + [[subItems objectAtIndex:currentExpandedIndex] count];
    
     //for Getting parent cell index
    NSIndexPath *index = [NSIndexPath indexPathForRow:currentExpandedIndex inSection:0];
   // protypeTableCell *parentCell = (protypeTableCell *) [self.songTableView cellForRowAtIndexPath:index];
    
    if (isChild) {
        NSLog(@"A child was tapped, do whatever you wanna do");
        previousIndexPath = indexPath;
         NSIndexPath* newIndexPath = [NSIndexPath indexPathForRow:previousIndexPath.row-1 inSection:previousIndexPath.section];
        [tableView selectRowAtIndexPath:newIndexPath animated:YES scrollPosition:0];
    }
    else
    {
        if(parentSelectedCount == 0)
        {
        parentSelectedCount++;
        }
        else if ([indexPath isEqual:index]) // till ios 7 we have two compare nsindex like this ,avoid "==".
        {
            NSLog(@"reload");
            parentSelectedCount = 0;
            [tableView reloadData];
            //[tableView reloadRowsAtIndexPaths:@[index] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    }
   
    
    [self.songTableView beginUpdates];
    
    if (currentExpandedIndex == indexPath.row) {
        [self collapseSubItemsAtIndex:currentExpandedIndex];
        currentExpandedIndex = -1;
    }
    else {
        
        BOOL shouldCollapse = currentExpandedIndex > -1
        ;
        
        if (shouldCollapse) {
            [self collapseSubItemsAtIndex:currentExpandedIndex];
        }
        
        currentExpandedIndex = (shouldCollapse && indexPath.row > currentExpandedIndex) ? indexPath.row - [[subItems objectAtIndex:currentExpandedIndex] count] : indexPath.row;
        
        [self expandItemAtIndex:currentExpandedIndex];
    }
//    if (previousIndexPath != nil) {
//        NSIndexPath* newIndexPath = [NSIndexPath indexPathForRow:previousIndexPath.row-1 inSection:previousIndexPath.section];
//        //[tableView deselectRowAtIndexPath:newIndexPath animated:YES];
//        
//    }
    [self.songTableView endUpdates];
   
  
}

- (void)expandItemAtIndex:(int)index {
    NSMutableArray *indexPaths = [NSMutableArray new];
    NSArray *currentSubItems = [subItems objectAtIndex:index];
    int insertPos = index + 1;
    for (int i = 0; i < [currentSubItems count]; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:insertPos++ inSection:0]];
    }
    [self.songTableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    [self.songTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
   // [indexPaths release];
}

- (void)collapseSubItemsAtIndex:(int)index {
    NSMutableArray *indexPaths = [NSMutableArray new];
    
    for (int i = index + 1; i <= index + [[subItems objectAtIndex:index] count]; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i inSection:0]];
    }
    [self.songTableView deleteRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationFade];
    
    //[indexPaths release];
}
#pragma mark - Share Button Action
- (IBAction)shareBtnAction:(id)sender {
    NSLog(@"this is share action");
}
@end
