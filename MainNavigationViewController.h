//
//  MainNavigationViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MainNavigationViewControllerDelegate <NSObject>

@end

@interface MainNavigationViewController : UIViewController <UIPageViewControllerDelegate,UIPageViewControllerDataSource,UIScrollViewDelegate>{
    NSArray *viewControllerArray;
    
}

//@property (nonatomic, strong) NSMutableArray *viewControllerArray;
@property (nonatomic, weak) id<MainNavigationViewControllerDelegate> navDelegate;
//@property (nonatomic, strong) UIView *selectionBar;
//@property (nonatomic, strong)UIPanGestureRecognizer *panGestureRecognizer;
@property (nonatomic, strong)UIPageViewController *pageController;
//@property (nonatomic, strong)UIView *navigationView;
//@property (nonatomic, strong)NSArray *buttonText;

-(void)viewToPresent:(int)_index withDictionary:(NSDictionary*)_dict;


@end
