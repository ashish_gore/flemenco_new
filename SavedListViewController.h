//
//  SavedListViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AVFoundation/AVFoundation.h>
#include <MediaPlayer/MPVolumeView.h>
#include <MediaPlayer/MPMusicPlayerController.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>

@interface SavedListViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,AVAudioPlayerDelegate>
{
    NSArray *topItems;
    NSMutableArray *subItems; // array of arrays
    NSMutableArray *songList;
    
    
    BOOL recordButtonEnabled;
    BOOL increaseChildRowHieght;
    BOOL dragEnabled;
    
    int currentExpandedIndex;
    int buttonCount;
    int parentSelectedCount;
    int playBtnCount;
    
    UIButton *trashButton;
    UIButton *firstKnob;
    UIButton *secondKnob;
    UIButton *thirdKnob;
    UIButton *forthKnob;
    
    CGPoint firstKnobCentre;
    CGPoint secondKnobCentre;
    CGPoint thirdKnobCentre;
    CGPoint forthKnobCentre;
    
    NSIndexPath *previousIndexPath;
    
    
    
    
}

@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;
@property (strong, nonatomic) IBOutlet UITableView *songTableView;
@property (strong, nonatomic) NSMutableDictionary *musicFileDict;
@property (strong, nonatomic)  AVAudioPlayer *recAudioPlayer;



- (IBAction)shareBtnAction:(id)sender;

@end
