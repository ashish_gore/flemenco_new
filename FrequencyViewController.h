//
//  FrequencyViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 05/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FrequencyViewController : UIViewController

- (IBAction)onTapBack:(id)sender;
@end
