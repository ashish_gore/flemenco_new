//
//  RecordViewController.h
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#include <AVFoundation/AVFoundation.h>
#include <MediaPlayer/MPVolumeView.h>
#include <MediaPlayer/MPMusicPlayerController.h>
#import <MediaPlayer/MediaPlayer.h>
#import <AudioToolbox/AudioToolbox.h>
@class MainNavigationViewController;

@interface RecordViewController : UIViewController<UIAlertViewDelegate,UIGestureRecognizerDelegate,iCarouselDataSource, iCarouselDelegate,AVAudioPlayerDelegate>{
    // Music File
    
    NSArray *currentOutputs;
    IBOutlet UIImageView *mic1;
    IBOutlet UIImageView *mic2;
    IBOutlet UIImageView *mic3;
    IBOutlet UIImageView *mic4;
    IBOutlet UIImageView *mic5;
    IBOutlet UIImageView *mic6;
    IBOutlet UIImageView *mic7;
    IBOutlet UIImageView *mic8;
    IBOutlet UIImageView *mic9;
    IBOutlet UIImageView *mic10;
    NSArray *micArray;
    
    IBOutlet UIImageView *circle1;
    IBOutlet UIImageView *circle2;
    IBOutlet UIImageView *circle3;
    IBOutlet UIImageView *circle4;
    IBOutlet UIImageView *circle5;
    IBOutlet UIImageView *circle6;
    IBOutlet UIImageView *circle7;
    IBOutlet UIImageView *circle8;
    IBOutlet UIImageView *circle9;
    IBOutlet UIImageView *circle10;
    IBOutlet UIImageView *circle11;
    IBOutlet UIImageView *circle12;
    // End here
    
    int bpmValue;
    int beatCount;
    int isStopped;
}

@property (strong, nonatomic) IBOutlet UIView *elementsView;
@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@property (strong, nonatomic) IBOutlet UIButton *playBtn;
@property (strong, nonatomic) IBOutlet UIButton *stopBtn;
@property (strong, nonatomic) IBOutlet UISlider *volumeSlider;      		
@property (strong, nonatomic) IBOutlet UISlider *bpmSlider;
@property (strong, nonatomic) IBOutlet UIScrollView *topScrollView;
@property (strong, nonatomic) IBOutlet UIView *recordView;
@property (strong, nonatomic) IBOutlet UIView *bpmView;
@property (strong, nonatomic) IBOutlet UIView *micView;
@property (strong, nonatomic) IBOutlet UIView *beatsView;
@property (strong, nonatomic) IBOutlet UILabel *playTimerBtn;
@property (strong, nonatomic) IBOutlet UILabel *playStopBtn;


@property (weak, nonatomic) IBOutlet UIButton *dronBtn;
//@property (weak, nonatomic) IBOutlet UIView *dronePickerLayer;
@property (weak, nonatomic) IBOutlet UIPickerView *dronePickerView;
//@property (weak, nonatomic) IBOutlet UILabel *droneTopLabel;
//@property (weak, nonatomic) IBOutlet UILabel *droneLabel;
//@property (weak, nonatomic) IBOutlet UILabel *droneBottomLabel;
@property (strong, nonatomic) IBOutlet UIView *topPickerView;
@property (weak, nonatomic) IBOutlet UIView *bottomPickerView;
@property (strong, nonatomic) NSDictionary *genreIdDict;
@property (readwrite, nonatomic) int genreIdSelected;
@property (readwrite, nonatomic) int bpmDefaultFlag;

// Sajiv Functionality Elements

@property (strong, nonatomic) IBOutlet UILabel *bpmTxt;
@property (strong, nonatomic) IBOutlet UILabel *recordTimerText;
@property (strong, nonatomic)  NSTimer *playTimer, *recordTimer;
@property (strong, nonatomic)  NSString *bpmString;
@property (strong, nonatomic)  AVAudioPlayer *audioPlayer;
@property (strong, nonatomic)  AVAudioPlayer *audioPlayerClap1;
@property (strong, nonatomic)  AVAudioPlayer *audioPlayerClap2;
@property (strong, nonatomic)  AVAudioPlayer *audioPlayerClap3;
@property (strong, nonatomic)  AVAudioPlayer *audioPlayerClap4;
@property (strong, nonatomic)  AVAudioRecorder *recorder;
@property (strong, nonatomic)  AVAudioSession *session;
@property (strong, nonatomic)  AVAudioSessionPortDescription *input;
@property (strong, nonatomic)  AVAudioSessionPortDescription *output;
@property (strong, nonatomic)  AVAudioSessionPortDescription *myPort;
@property (strong, nonatomic)  MPVolumeView *volumeView;
@property (strong, nonatomic)  MPMusicPlayerController *musicPlayer;

- (void) playMemos:(NSString *)memo;
- (void) playRhythmsWithBeats:(AVAudioPlayer *)audioPlayer:(int)beatNumber;
- (void) playSelectedRhythms:(NSString*)playRythm;
- (void) stopSelectedRhythms;
- (float) bpmForSelectedRythm:(NSString*)_rythm;
- (void) calculateMicGain:(int) gain;

@property (strong, nonatomic) MainNavigationViewController *myNavigationController;
// Ends here

@property (nonatomic, retain)	AVAudioPlayer			*appSoundPlayer;
@property (nonatomic, retain)	UIImage					*noArtworkImage;
@property (nonatomic, retain)	UIBarButtonItem			*artworkItem;
@property (nonatomic, retain)	UILabel					*nowPlayingLabel;
@property (strong, nonatomic) IBOutlet UIButton *instBtn1;
@property (strong, nonatomic) IBOutlet UIButton *instBtn2;
@property (strong, nonatomic) IBOutlet UIButton *instBtn3;
@property (strong, nonatomic) NSString *droneType;

- (IBAction)onTapClap1Btn:(id)sender;
- (IBAction)onTapClap2Btn:(id)sender;
- (IBAction)onTapClap3Btn:(id)sender;
- (IBAction)onTapClap4Btn:(id)sender;
- (IBAction)onTapPlayBtn:(id)sender;
- (IBAction)onTapStopBtn:(id)sender;

- (IBAction)OnChangeVolumeSlider:(id)sender;
- (IBAction)onChangeBPM:(id)sender;
- (IBAction)onTapFringe:(id)sender;

-(void)setDataToUIElements:(int)_cnt;
@end
