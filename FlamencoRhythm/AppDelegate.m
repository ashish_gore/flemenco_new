//
//  AppDelegate.m
//  FlamencoRhythm
//
//  Created by Mayank Mathur on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import "AppDelegate.h"

#import "ViewController.h"
#import "RecordViewController.h"
#import "SavedListViewController.h"
#import "RhythmClass.h"

@interface AppDelegate (){
    
}

@end

@implementation AppDelegate
@synthesize myNavigationController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    UIPageViewController *pageController = [[UIPageViewController alloc] initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal options:nil];
//    
//    myNavigationController = [[MainNavigationViewController alloc]initWithRootViewController:pageController];
    
    _latestRhythmClass = [[RhythmClass alloc]init];
    
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    myNavigationController = [mainStoryboard instantiateViewControllerWithIdentifier:@"navigationVC"];
    
//    // View Controllers
//    ViewController *firstVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"firstVC"];
//    RecordViewController *secondVC= [mainStoryboard instantiateViewControllerWithIdentifier:@"secondVC"];
//    //SavedListViewController *thirdVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"thirdVC"];
//    AccordionTableViewController *thirdTableVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"thirdVC"];
//
//    [myNavigationController.viewControllerArray addObjectsFromArray:@[firstVC,secondVC,thirdTableVC]];
//    [myNavigationController setNavigationBarHidden:YES animated:YES];
    self.window.rootViewController = myNavigationController;
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
