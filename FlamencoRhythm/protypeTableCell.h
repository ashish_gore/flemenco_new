//
//  protypeTableCell.h
//  FlamencoRhythm
//
//  Created by Ashish Gore on 04/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface protypeTableCell : UITableViewCell
{
    //int tapCount;
}
@property (nonatomic) int tapCount;
@property (strong, nonatomic) IBOutlet UILabel *songNameLbl;
@property (strong, nonatomic) IBOutlet UIButton *editBtn;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;
@property (strong, nonatomic) IBOutlet UILabel *TotalTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel *songDetailLbl;
@property (strong, nonatomic) IBOutlet UIButton *closeBtn;

@property (strong, nonatomic) IBOutlet UIView *recorderView;
@property (strong, nonatomic) IBOutlet UIButton *trashDefaultBtn;
@property (strong, nonatomic) IBOutlet UIButton *firstVolumeKnob;
@property (strong, nonatomic) IBOutlet UIButton *secondVolumeKnob;
@property (strong, nonatomic) IBOutlet UIButton *thirdVolumeKnob;
@property (strong, nonatomic) IBOutlet UIButton *fourthVolumeKnob;
@property (strong, nonatomic) IBOutlet UIButton *playRecBtn;
@property (strong, nonatomic) IBOutlet UILabel *maxRecDurationLbl;



@end
