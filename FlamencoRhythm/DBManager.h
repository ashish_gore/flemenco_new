//
//  DBManager.h
//  FlamencoRhythm
//
//  Created by Sajiv Nair on 16/03/15.
//  Copyright (c) 2015 Intelliswift Software Pvt. Ltd. All rights reserved.
//

#ifndef FlamencoRhythm_DBManager_h
#define FlamencoRhythm_DBManager_h

#import <sqlite3.h>

#endif

@interface DBManager : NSObject

@property (nonatomic, strong) NSString *documentsDirectory;
@property (nonatomic, strong) NSString *databaseFilename;

-(DBManager*)initWithDatabaseFilename:(NSString *)dbFilename;
-(void)copyDatabaseIntoDocumentsDirectory;

- (BOOL)insertDataToRecordingInDictionary:(NSDictionary*)dict;
- (BOOL)saveData1;
-(int)getRowCount:(NSString*)tableName;
-(BOOL)createRecordingTable;
-(BOOL)createGenreTable;
-(BOOL)createRhythmTable;
-(NSMutableArray *)getRhythmsFromGenre:(NSString*)genre;
-(NSMutableArray*)getRhythmRecords:(NSNumber*)genreId;
-(NSDictionary*)getAudioFileRecords;

// Rasool's Method
-(void)isDBOpened;
-(NSMutableArray *)getAllSectionDetails:(NSString*)tableName;
- (NSMutableArray *)getAllRecordingData;
@end
